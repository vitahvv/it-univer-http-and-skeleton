import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './client.component';
import {ClientRouting} from './client.routing';
import {AppSharedModule} from '../shared/app-shared.module';

@NgModule({
  imports: [
    CommonModule, ClientRouting, AppSharedModule
  ],
  declarations: [ClientComponent],
  providers: []
})
export class ClientModule { }
